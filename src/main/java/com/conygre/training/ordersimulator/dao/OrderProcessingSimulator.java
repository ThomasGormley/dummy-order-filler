package com.conygre.training.ordersimulator.dao;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.conygre.training.events.Event;
import com.conygre.training.events.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;


/**
 * This class is a test harness class - it only exists for testing.
 * <p>
 * This is SIMULATING sending trade orders to an exchange for them to be filled.
 * <p>
 * This class uses the following codes to represent the state of an order:
 * <p>
 * 0 : initialized i.e. has not yet been processed at all
 * 1 : processing  i.e. the order has been sent to an exchange, we are waiting for a response
 * 2 : filled i.e. the order was successfully placed
 * 3 : rejected i.e. the order was not accepted by the trading exchange
 * <p>
 * The above are JUST SUGGESTED VALUES, you can change or improve as you see think is appropriate.
 */
@Repository
public class OrderProcessingSimulator {

    // Standard mechanism for logging with spring boot - org.slf4j is built-in to spring boot
    private static final Logger LOG = LoggerFactory.getLogger(OrderProcessingSimulator.class);
    private final String INITALIZED = "initalized";
    private final String PROCESSING = "processing";
    private final String SUCCESS = "success";
    private final String FAILURE = "failure";

    // You'll need to change these to match whatever you called your table and status_code field
    // You may also need to change the database name in application-mysql.properties and application-h2.properties
    // The default database name that's used here is "appDB"
    @Value("${mysql.table.name}")
    private String TABLE;

    @Value("${mysql.statuscode.colname}")
    private String STATUS_CODE;

    @Value("${percent.failures}")
    private int percentFailures = 10;

    @Autowired
    private JdbcTemplate template;

    /**
     * Any record in the configured database table with STATE=0 (init)
     * <p>
     * Will be changed to STATE=1 (processing)
     * <p>
     * Note the @Scheduled : this tells spring to automatically call this method on a schedule
     * <p>
     * 10000 indicates to spring that this method should be called every 10,000 milliseconds (10s)
     */
    @Scheduled(fixedRateString = "${proc.rate.ms:10000}")
    public int findTradesForProcessing() {
        String sqlFindTradeIDsWithStatusZero = "SELECT StockID FROM " + TABLE + " WHERE " + STATUS_CODE + "=0";
        List<Map<String, Object>> tradesWithStatusZero = template.queryForList(sqlFindTradeIDsWithStatusZero);
        LOG.debug(String.valueOf(tradesWithStatusZero));


        String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=1 WHERE " + STATUS_CODE + "=0";
        int numberChanged = template.update(sql);

        tradesWithStatusZero.forEach(tradeID -> {
            Long tradeIDAsLong = Long.valueOf(String.valueOf(tradeID.get("StockID")));
            LOG.debug("Emitting " + INITALIZED + " event on tradeID: " + tradeIDAsLong);

            Event.emitEvent(tradeIDAsLong, INITALIZED, PROCESSING);
        });

        LOG.debug("Updated [" + numberChanged + "] order from initialized (0) TO processing (1)");
        return numberChanged;
    }

    /**
     * Anything in the configured database table with STATE=1 (processing)
     * <p>
     * Will be changed to STATE=2 (filled) OR STATE=3 (rejected)
     * <p>
     * This method uses a random number to determine when trades are rejected.
     * <p>
     * Note the @Scheduled : this tells spring to automatically call this method on a schedule
     * <p>
     * 10000 indicates to spring that this method should be called every 10,000 milliseconds (10s)
     */
    @Scheduled(fixedRateString = "${fill.rate.ms:15000}")
    public int findTradesForFillingOrRejecting() {
        int totalChanged = 0;
        int lastChanged = 0;

        do {
            lastChanged = 0;

            // use a random number to decide if we'll simulate success OR failure
            int randomInteger = new Random().nextInt(100);

            LOG.debug("Random number is [" + randomInteger +
                    "] , failure rate is [" + percentFailures + "]");

            if (randomInteger > percentFailures) {
                // Mark this one as success
                lastChanged = markTradeAsSuccessOrFailure(2);
                LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO success (2)");
            } else {
                // Mark this one as failure!!
                lastChanged = markTradeAsSuccessOrFailure(3);
                LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO failure (3)");
            }
            totalChanged += lastChanged;

        } while (lastChanged > 0);

        return totalChanged;
    }

    /*
     * Update a single record to success or failure
     */
    private int markTradeAsSuccessOrFailure(int successOrFailure) {
        String successOrFailureEvent = successOrFailure == 2 ? SUCCESS : FAILURE;
        String sqlFindTradeIDsWithStatusZero = "SELECT StockID FROM " + TABLE + " WHERE " + STATUS_CODE + "=" + "1 limit 1";
        Long tradeIDToUpdate = null;
        try {
            tradeIDToUpdate = template.queryForObject(sqlFindTradeIDsWithStatusZero, Long.class);
        } catch (EmptyResultDataAccessException ex) {
            LOG.debug("No trades to update");
        }
        String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=" +
                successOrFailure + " WHERE " + STATUS_CODE + "=1 limit 1";
        int rowsChanged = template.update(sql);
        if (rowsChanged > 0) {
            LOG.debug("Emitting " + successOrFailureEvent + " event on tradeID: " + tradeIDToUpdate);
            Event.emitEvent(tradeIDToUpdate, PROCESSING, successOrFailureEvent);
        }
        return rowsChanged;
    }

    public void setPercentFailures(int percentFailures) {
        this.percentFailures = percentFailures;
    }

}